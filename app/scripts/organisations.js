var organisation = {

    "AmnestyInternational" : {
        "img" : "http://www.welovead.com/upload/photo_db/2012/05/19/201205191320364966/960_960/201205191320364966.jpg",
        "name": "Amnesty International",
        "logo" : "https://www.aivl.be/sites/all/themes/amnesty7port/images/header-logo.gif",
        "donate" : "https://www.amnesty.org.uk/giving/donate/give-monthly",
        "facebook" : "https://www.facebook.com/amnestyglobal/?fref=ts",
        "twitter" : "https://twitter.com/amnesty",
        "site" : "https://www.amnesty.org/en/",
        "text" : "Amnesty International (commonly known as Amnesty and AI) is a non-governmental organisation focused on human rights with over 7 million members and supporters around the world. The stated objective of the organisation is \"to conduct research and generate action to prevent and end grave abuses of human rights, and to demand justice for those whose rights have been violated.\"",
        "continent" : ["Europe", "Afrika"],
        "class" : "AmnestyInternational"

    },

    "Unicef" : {
        "img" : "http://www.unicef.be/wp-content/uploads/2014/03/corporate-partners.jpg",
        "name": "Unicef",
        "logo" : "http://www.unicef.org/images/global_logo_2013.png",
        "donate" : "http://www.unicef.org.uk/landing-pages/donate-syria/",
        "facebook" : "https://www.facebook.com/unicef/?fref=ts",
        "twitter" : "https://twitter.com/UNICEF?lang=nl",
        "site" : "http://www.unicef.org/",
        "text" : "UNICEF is de kinderrechtenorganisatie van de Verenigde Naties. Als onderdeel van de VN ziet zij erop toe dat het internationale Verdrag voor de Rechten van het Kind overal ter wereld wordt nageleefd. UNICEF komt op voor de rechten van alle kinderen en heeft een opdracht: ervoor zorgen dat elk land de rechten van kinderen respecteert en naleeft.",
        "continent" : [""],
        "class" : "Unicef"
    }

};

var test = document.getElementById("test");

test.innerHTML = organisation.Unicef.name + "<br />"
+ organisation.Unicef.logo
;