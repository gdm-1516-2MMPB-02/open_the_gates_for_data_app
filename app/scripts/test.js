/**
 * Created by brambilliet on 12/12/15.
 */




$(function() {
    $('#map').vectorMap({
        map: 'world_mill',
        backgroundColor:['#3b484f'],
        onRegionClick: function(event, code){
            if (code == "NA") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(WWF) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(BirdLife) + template(NatureServe) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
                $("#verbergen").toggleClass();
            } else if (code == "SA") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(BirdLife) + template(TheMountainInstitute) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth);
                $("#verbergen").toggleClass();
            } else if (code == "EU") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(Oxfam) + template(WWF) + template(RedCross) + template(GreenPeace) + template(Gaia) + template(SeaShepherd) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);;
                $("#verbergen").toggleClass();
            } else if (code == "AF") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(GlobalWitness) + template(BirdLife) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalHand) + template(InternationalResuceCommittee);
                $("#verbergen").toggleClass();
            } else if (code == "AS") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(WomensAidOrganisation) + template(GlobalWitness) + template(BirdLife) + template(TheMountainInstitute) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
                $("#verbergen").toggleClass();
            } else if (code == "OC") {
                document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(RedCross) + template(GreenPeace) + template(GlobalWitness) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
                $("#verbergen").toggleClass();
            }
                },

        series: {
            regions: [{
                values: {
                    'NA': '#eb7d7d',
                    'SA': '#24a38b',
                    'EU':'#b3d786',
                    'AF':'#fddb77',
                    'AS':'#6cc0eb',
                    'OC':'#eaac5c'

                },
                attribute: 'fill'
            }]
        },
    });


});















































/*

 //SAFE//

 $(function() {
 $('#map').vectorMap({
 map: 'world_mill',
 backgroundColor:['#3b484f'],

series: {
    regions: [{
        values: {
            'NA': '#eb7d7d',
            'SA': '#24a38b',
            'EU':'#b3d786',
            'AF':'#fddb77',
            'AS':'#6cc0eb',
            'OC':'#eaac5c',

        },
        attribute: 'fill',
    }]
},
});


});

 */