/*
 *	Description: Ghent Trees Application
 *	Modified: 10-11-2015
 *	Version: 1.0.0
 *	Author: Philippe De Pauw - Waterschoot
 * 	-----------------------------------------------
 *	Ghent Bomen Inventaris API: http://datatank.stad.gent/4/milieuennatuur/bomeninventaris.json
 */

// Anonymous function executed when document loaded
(function() {

	// Describe an App object with own functionalities
	var App = {
		init: function() {
			this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners
			this.registerWindowListeners();// Register All Navigation Toggle Listeners
		},
		registerNavigationToggleListeners: function() {
			var toggles = document.querySelectorAll('.navigation-toggle');

			if(toggles != null && toggles.length > 0) {
				var toggle = null;

				for(var i = 0; i < toggles.length; i++ ) {
					toggle = toggles[i];
					toggle.addEventListener('click', function(ev) {
						ev.preventDefault();

						document.querySelector('body').classList.toggle(this.dataset.navtype);

						return false;
					});
				}
			}
		},
		registerWindowListeners: function() {
			window.addEventListener('resize', function(ev) {
				if(document.querySelector('body').classList.contains('offcanvas-open')) {
					document.querySelector('body').classList.remove('offcanvas-open');
				}

				if(document.querySelector('body').classList.contains('headernav-open')) {
					document.querySelector('body').classList.remove('headernav-open');
				}
			});
		}
	};

	App.init();// Intialize the application

})();


(function(){

	var app = {
		init:function(){
			var url_populatie = "http://api.worldbank.org/countries/ECS/indicators/SP.POP.TOTL?format=jsonP&prefix=jsonp_callback&date=2010";
			this.loadFromAPI(url_populatie);
		},
		loadFromAPI: function(url){
			utils.getJSONPByPromise(url).then(
					function(data){
						if (data[1]!=null){
							console.log(data[1]);
							document.getElementById("test").innerHTML = data[1].query.results.value;
						}
					}
			);
		}

	};
	app.init();

})();