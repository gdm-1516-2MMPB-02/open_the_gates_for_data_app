var source   = document.querySelector("#users-template").innerHTML;
var template = Handlebars.compile(source);

var AmnestyInternational = [

    {
        "img" : "http://www.welovead.com/upload/photo_db/2012/05/19/201205191320364966/960_960/201205191320364966.jpg",
        "name": "Amnesty International",
        "logo" : "https://www.aivl.be/sites/all/themes/amnesty7port/images/header-logo.gif",
        "donate" : "https://www.amnesty.org.uk/giving/donate/give-monthly",
        "facebook" : "https://www.facebook.com/amnestyglobal/?fref=ts",
        "twitter" : "https://twitter.com/amnesty",
        "site" : "https://www.amnesty.org/en/",
        "text" : "Amnesty International (commonly known as Amnesty and AI) is a non-governmental organisation focused on human rights with over 7 million members and supporters around the world. The stated objective of the organisation is \"to conduct research and generate action to prevent and end grave abuses of human rights, and to demand justice for those whose rights have been violated.\"",
        "continent" : ["Europe", "Afrika"],
        "link" : "http://www.arteveldehogeschool.be/campusGDM/studenten_201516/kevidevu/open_the_gates_for_data_app/app/organisation/organisations/organisatie.html"
    }
    ];

var Unicef = [

    {
        "img" : "http://www.unicef.be/wp-content/uploads/2014/03/corporate-partners.jpg",
        "name": "Unicef",
        "logo" : "http://www.unicef.org/images/global_logo_2013.png",
        "donate" : "http://www.unicef.org.uk/landing-pages/donate-syria/",
        "facebook" : "https://www.facebook.com/unicef/?fref=ts",
        "twitter" : "https://twitter.com/UNICEF?lang=nl",
        "site" : "http://www.unicef.org/",
        "text" : "UNICEF is de kinderrechtenorganisatie van de Verenigde Naties. Als onderdeel van de VN ziet zij erop toe dat het internationale Verdrag voor de Rechten van het Kind overal ter wereld wordt nageleefd. UNICEF komt op voor de rechten van alle kinderen en heeft een opdracht: ervoor zorgen dat elk land de rechten van kinderen respecteert en naleeft.",
        "continent" : [""]
    }
    ];

var DoctorsWithoutBorders = [
    {
        "img" : "http://media.npr.org/assets/img/2014/10/09/doctors_wide-0748baa432d0f51241d307eee225061dd545aa5a.jpg",
        "name": "Doctors Without Borders",
        "logo" : "https://pbs.twimg.com/profile_images/640822551938134016/BJsxsEgT_400x400.jpg",
        "donate" : "https://donate.doctorswithoutborders.org/onetime.cfm",
        "facebook" : "https://www.facebook.com/msf.english/?fref=ts",
        "twitter" : "https://twitter.com/msf_field?lang=nl",
        "site" : "http://www.doctorswithoutborders.org/",
        "text" : "Médecins Sans Frontières (MSF) (pronounced [medsɛ̃ sɑ̃ fʁɔ̃tjɛʁ] ( listen)), or Doctors Without Borders, is an international humanitarian-aid non-governmental organization (NGO) and Nobel Peace Prize laureate, best known for its projects in war-torn regions and developing countries facing endemic diseases. It was founded in France.[1] The organization is known in most of the world by its localized name or simply as MSF; in Canada and the United States the name Doctors Without Borders is commonly used. ",
        "continent" : [""]
    }
    ];


var Oxfam = [
    {
        "img" : "https://www.oxfam.org/sites/www.oxfam.org/files/styles/carousel-full/public/hp_main.jpg?itok=IPBtIi8X",
        "name": "Oxfam",
        "logo" : "http://www.oxfamwereldwinkels.be/sites/all/themes/xtheme/logo.png",
        "donate" : "http://www.oxfamwereldwinkels.be/nl/steunen",
        "facebook" : "https://www.facebook.com/Oxfam-International-409778779230451/?fref=ts",
        "twitter" : "https://twitter.com/Oxfam?lang=nl",
        "site" : "http://www.oxfam.be/",
        "text" : "Oxfam is an international confederation of 17 organisations working in approximately 94 countries worldwide to find solutions to poverty and what it considers injustice around the world.[1] In all Oxfam's actions, the ultimate goal is to enable people to exercise their rights and manage their own lives. ",
        "continent" : [""]
    }
    ];

var WWF = [
    {
        "img" : "http://theglobaljournal.s3.amazonaws.com/photos%2F2012%2F01%2F9ffa68b09a00af6b.png",
        "name": "WWF",
        "logo" : "https://upload.wikimedia.org/wikipedia/en/thumb/2/24/WWF_logo.svg/263px-WWF_logo.svg.png",
        "donate" : "https://support.worldwildlife.org/site/SPageServer?pagename=donate_to_charity",
        "facebook" : "https://www.facebook.com/WWF/?fref=ts",
        "twitter" : "https://twitter.com/WWF?lang=nl",
        "site" : "http://www.wwf.eu/",
        "text" : "The World Wide Fund for Nature (WWF) is an international non-governmental organization founded on April 29, 1961, working in the field of the biodiversity conservation, and the reduction of humanity's footprint on the environment. It was formerly named the World Wildlife Fund, which remains its official name in Canada and the United States.",
        "continent" : [""]
    }
    ];

var FoodAid = [
    {
        "img" : "https://www.aei.org/wp-content/uploads/2013/06/Kenya_food_aid_6860085138.jpg",
        "name": "Food Aid",
        "logo" : "http://food-aid.org/shop/ccdata/images/Food%20Aid%20Logo-01.png",
        "donate" : "",
        "facebook" : "https://www.facebook.com/Food-Aid-Foundation-602936956410922/?fref=ts",
        "twitter" : "https://twitter.com/Food_Aid?lang=nl",
        "site" : "http://foodaid.org/",
        "text" : "",
        "continent" : [""]
    }
    ];

var PlanInternational = [
    {
        "img" : "https://plan-international.org/sites/files/plan/styles/s__map_image/public/field/field_map_point_image/ben-map_banner-children_hands_up.jpg?itok=Lw36RfyR&timestamp=1439903730",
        "name": "Plan International",
        "logo" : "https://pbs.twimg.com/profile_images/669815781094477824/4bVdt6Jl_400x400.jpg",
        "donate" : "https://plan-international.org/make-donation",
        "facebook" : "https://www.facebook.com/planinternational/?fref=ts",
        "twitter" : "https://twitter.com/PlanGlobal?lang=nl",
        "site" : "https://plan-international.org/",
        "text" : "Plan International is an international development organisation operating in 51 countries across Africa, the Americas, and Asia to promote and protect children's rights.[1] The nonprofit organisation is one of the world's largest child-centered community development organisations, working in 58,000 communities with 600,000 volunteers to improve the quality of life for more than 56 million children.",
        "continent" : [""]
    }
    ];

var RedCross = [
    {
        "img" : "https://www.icrc.org/sites/default/files/styles/home_carousel_slide/public/home_carousel_slide/image/slovenia-voyage-unknown-163-carousel.jpg?itok=2sjaDUjP",
        "name": "Red Cross",
        "logo" : "http://redcross.eu/en/upload/images/2013/RC_EU_OFFICE_Logo_RVB_230x60.jpg",
        "donate" : "http://www.ifrc.org/global/rw/protecthumanity/#intro",
        "facebook" : "https://www.facebook.com/RedCrossEU/?fref=ts",
        "twitter" : "https://twitter.com/RedCrossEU",
        "site" : "http://www.redcross.eu/en/",
        "text" : "The International Red Cross and Red Crescent Movement is an international humanitarian movement with approximately 97 million volunteers, members and staff worldwide[2] which was founded to protect human life and health, to ensure respect for all human beings, and to prevent and alleviate human suffering.",
        "continent" : [""]
    }
    ];

var GreenPeace = [
    {
        "img" : "http://media.nu.nl/m/m1mxaceaghsc_wd1280.jpg/celstraf-activisten-zat-niet-in-risicoanalyse-greenpeace.jpg",
        "name": "Green Peace",
        "logo" : "http://www.greenpeace.org/usa/wp-content/uploads/2015/07/Greenpeace-logo.png",
        "donate" : "https://donate.greenpeace.org/hpp/pay.shtml",
        "facebook" : "https://www.facebook.com/greenpeace.international/?fref=ts",
        "twitter" : "https://twitter.com/Greenpeace",
        "site" : "http://www.greenpeace.org/international/en/",
        "text" : "Greenpeace is a non-governmental[3] environmental organization with offices in over forty countries and with an international coordinating body in Amsterdam, the Netherlands.[4] Greenpeace states its goal is to \"ensure the ability of the Earth to nurture life in all its diversity\" and focuses its campaigning on worldwide issues such as climate change, deforestation, overfishing, commercial whaling, genetic engineering, and anti-nuclear issues.",
        "continent" : [""]
    }
    ];

var WomensAidOrganisation = [
    {
        "img" : "http://www.wao.org.my/banner/rotator/568a229378d12fa14499c34e3ac23abf.jpg",
        "name": "Women's Aid Organisation",
        "logo" : "http://www.wao.org.my/images/702001.png",
        "donate" : "http://www.wao.org.my/Contact+Us_29_29_1.htm",
        "facebook" : "https://www.facebook.com/pages/Womens-Aid-Organisation/119966998048915?fref=ts",
        "twitter" : "",
        "site" : "http://www.wao.org.my/",
        "text" : "A Protem committee, headed by Puan Sri E.N. Chong, had its first meeting in November 1981. It took 9 months to lay the groundwork and to form a core group of volunteers. These pioneer volunteers worked as a collective to formulate the operating principles of self help and self empowerment for battered women who turned to WAO for help. In June 1982, WAO received temporary registration as a society and a single storey house was rented as WAO's refuge and office premises. The Malay Mail published the first article about the planned Refuge. On a Saturday morning in September 1982, a call came in: \"She has left her husband with her two children, could she stay for a while?\" This woman and her two children were WAO's first residents.",
    "continent" : ["Asia"]
}
    ];
var GlobalWitness = [
{
    "img" : "https://www.globalwitness.org/media/images/Corbis_offshore_oil_Grafton_Marsh.2e16d0ba.fill-1600x900_juxdGGU.jpg",
    "name": "Global Witness",
    "logo" : "http://drctimbertracker.globalwitness.org/img/gw_logo.png",
    "donate" : "https://www.globalwitness.org/en/donate/",
    "facebook" : "https://www.facebook.com/GlobalWitness",
    "twitter" : "https://twitter.com/global_witness",
    "site" : "https://www.globalwitness.org/",
    "text" : "For two decades we’ve been campaigning for full transparency in the mining, logging, oil and gas sectors, so that citizens who own those resources can benefit fairly from them, now and in future. We believe that the only way to protect peoples’ rights to land, livelihoods and a fair share of their national wealth is to demand total transparency in the resources sector, sustainable and equitable resources management, and stopping the international financial system from propping up resource-related corruption.",
    "continent" : [""]
}
    ];

var BirdLife = [
{
    "img" : "http://www.birdlife.org/sites/default/files/styles/fullsize_940x400/public/featured/banner.png?itok=hphGDy-0",
    "name": "Birdlife International",
    "logo" : "http://www.naturekenya.org/sites/default/files/New%20BirdLife.png",
    "donate" : "http://www.birdlife.org/worldwide/support-us/donate",
    "facebook" : "https://www.facebook.com/BirdLifeInternational",
    "twitter" : "https://twitter.com/BirdLife_News",
    "site" : "http://www.birdlife.org/",
    "text" : "BirdLife International is the world’s largest nature conservation Partnership. Together we are 119 BirdLife Partners worldwide from 117 country/territories. We are driven by our belief that local people, working for nature in their own places but connected nationally and internationally through our global Partnership, are the key to sustaining all life on this planet. This unique local-to-global approach delivers high impact and long-term conservation for the benefit of nature and people.",
    "continent" : [""]
}
    ];

var Gaia = [
{
    "img" : "http://www.gaia.be/sites/default/files/styles/homepage_highlight_large/public/highlight/large/highlight_0.jpg?itok=2P113NrK",
    "name": "Gaia",
    "logo" : "https://www.vlaamsparlement.be/sites/default/files/media/Logos/GAIA_Pantone259-square.png",
    "donate" : "http://www.gaia.be/nl/gift",
    "facebook" : "https://www.facebook.com/gaia.be",
    "twitter" : "https://twitter.com/gaiabrussels",
    "site" : "http://www.gaia.be/",
    "text" : "Sinds 1992 komt GAIA op voor dierenbelangen en ijveren we onafgebroken voor een steeds betere bescherming van levende wezens die pijn en angst kunnen voelen. Daarom voert GAIA actie en campagnes tegen georganiseerde dierenmishandeling, menselijke praktijken die dieren op grote schaal doen lijden.",
    "continent" : [""]
}
    ];

var SeaShepherd = [
{
    "img" : "http://www.seashepherd.be/media/rokgallery/8/8da2bd3d-1fbd-46d2-d6bb-766a6c0f2739/e6bf3f25-1a22-4d0b-ea5c-b20a85392528.jpg",
    "name": "Sea Shepherd",
    "logo" : "http://41.media.tumblr.com/819b63b4070d67f2519746f538063658/tumblr_inline_nnroadnwhH1t88obl_500.png",
    "donate" : "http://www.seashepherd.be/nl/steun-ons/doneer",
    "facebook" : "https://www.facebook.com/Sea-Shepherd-Belgium-159282000795381/?fref=ts",
    "twitter" : "https://twitter.com/seashepherd",
    "site" : "http://www.seashepherd.be/",
    "text" : "Sea Shepherd Conservation Society (SSCS) is een internationale non-profit organisatie met als doelstelling het wereldwijd beschermen van het leven in zee. De organisatie is in 1977 opgericht door de Canadese natuurbeschermer Paul Watson. Hij was de eerste mens die zich met een rubberboot tussen walvis en harpoen wierp. Met deze daad trok hij wereldwijd de aandacht. Ruim 35 jaar later zijn directe acties op zee nog steeds het handelsmerk van Sea Shepherd.",
    "continent" : ["Europe"]
}
    ];

var TheMountainInstitute = [
{
    "img" : "http://www.mountain.org/sites/default/files/imagecache/wn_hp_callout/a.png",
    "name": "The Mountain Institute",
    "logo" : "http://www.mountain.org/sites/default/files/wn_whiteoak_logo.gif",
    "donate" : "https://donatenow.networkforgood.org/themountaininstitute",
    "facebook" : "https://www.facebook.com/TheMountainInstitute",
    "twitter" : "https://twitter.com/themountaininst",
    "site" : "http://www.mountain.org/",
    "text" : "THE MOUNTAIN INSTITUTE (TMI) works hand-in-hand with remote mountain communities to address their most critical challenges. Our work revolves around three central themes: conservation of mountain ecosystems, sustainable economic development and support for unique mountain cultures. Since our founding in 1972, we have listened to the people who know mountains best — those who live there — and have worked closely with them to identify and implement solutions to improve their livelihoods and the health of their environments. The Mountain Institute works with academic and technical experts while partnering with government and development agencies. Our programs now reach more than half a million people each year. Approaches TMI has developed are now in use in mountainous countries around the world.",
    "continent" : [""]
}
    ];

var NatureServe = [
{
    "img" : "http://www.natureserve.org/sites/default/files/styles/featured_desktop/public/articles/featured/bird.jpg?itok=EFNCqyFS",
    "name": "NatureServe",
    "logo" : "http://www.natureserve.org/sites/default/files/default_images/logo.png",
    "donate" : "https://www.natureserve.org/civicrm/contribute/transact?reset=1&id=1",
    "facebook" : "https://www.facebook.com/natureserve",
    "twitter" : "https://twitter.com/natureserve",
    "site" : "http://www.natureserve.org/",
    "text" : "NatureServe is a non-profit organization that provides high-quality scientific expertise for conservation. Our dynamic and impactful data, tools, and resources help guide conservation action where it’s needed most. We envision a world where decision-makers fully understand the importance of science in identifying and protecting our precious animals, plants, and ecosystems.",
    "continent" : [""]
}
    ];

var TheWanderingSamaritan = [
{
    "img" : "https://www.thewanderingsamaritan.org/wp-content/uploads/2014/10/DUNK.jpg",
    "name": "The Wandering Samaritan",
    "logo" : "https://www.thewanderingsamaritan.org/wp-content/uploads/2015/05/Logo-200x200.png",
    "donate" : "https://www.thewanderingsamaritan.org/donate/",
    "facebook" : "https://www.facebook.com/thewanderingsamaritan",
    "twitter" : "https://twitter.com/tws_org",
    "site" : "https://www.thewanderingsamaritan.org/#home",
    "text" : "The Wandering Samaritan enables international travelers to create random acts of kindness and small scale humanitarian projects along their journeys",
    "continent" : [""]
}
    ];

var AdvocatsForWorldHealth = [
{
    "img" : "http://www.awhealth.org/wp-content/uploads/2013/09/timthumb-2.jpg",
    "name": "Advocats For World Health",
    "logo" : "http://www.repurposeproject.org/wp-content/uploads/2015/05/advocates-for-world-health.png",
    "donate" : "http://awhealth.org/monetary-donations/",
    "facebook" : "https://www.facebook.com/Advocates4WorldHealth",
    "twitter" : "https://twitter.com/AWHealth",
    "site" : "http://awhealth.org/",
    "text" : "Advocates for World Health is a not-for-profit corporation that recovers surplus medical products and distributes them to relief agencies working in developing nations.",
    "continent" : [""]
}
    ];

var InternationalHand = [
{
    "img" : "https://static.wixstatic.com/media/24f3ce_6157b442e84f4a4eb33844beb479ee81.jpg/v1/fit/w_1012,h_759,q_90,usm_0.66_1.00_0.01/24f3ce_6157b442e84f4a4eb33844beb479ee81.jpg",
    "name": "International Hand",
    "logo" : "http://donationsstatic.ebay.com/extend/logos/MF35542.gif",
    "donate" : "http://www.internationalhand.org/#!about1/c19iz",
    "facebook" : "https://www.facebook.com/internationalhand/",
    "twitter" : "",
    "site" : "http://www.internationalhand.org/",
    "text" : "International Hand Foundation (IHF) is a not-for-profit 501(c)(3) corporation, and our specific purpose is to provide charitable assistance to communities in developing countries that are suffering from extreme poverty. We seek to provide educational opportunities to children in those communities.",
    "continent" : [""]
}
    ];

var InternationalResuceCommittee = [
{
    "img" : "http://www.rescue.org/sites/default/files/homepage_slideshow/prefugeecrisis.jpg?1450482327",
    "name": "International Rescue Committee",
    "logo" : "http://www.rescue.org/sites/default/files/logo.gif",
    "donate" : "https://engage.rescue.org/donate",
    "facebook" : "https://www.facebook.com/InternationalRescueCommittee/",
    "twitter" : "https://twitter.com/theirc",
    "site" : "http://www.rescue.org/",
    "text" : "The International Rescue Committee (IRC) responds to the world’s worst humanitarian crises and helps people to survive and rebuild their lives. At work in over 40 countries and 25 U.S. cities to restore safety, dignity and hope, the IRC leads the way from harm to home.",
    "continent" : [""]
}
];

//organisations.sort(function(a, b){
//    if(a.name < b.name) return -1;
//    if(a.name > b.name) return 1;
//    return 0;
//});

document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(WomensAidOrganisation) + template(GlobalWitness) + template(BirdLife) + template(Gaia) + template(SeaShepherd) + template(TheMountainInstitute) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalHand) + template(InternationalResuceCommittee);

var noordAmerica = document.getElementById("noordAmerica");

noordAmerica.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(WWF) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(BirdLife) + template(NatureServe) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
    $("#verbergen").toggleClass();
});

var zuidAmerica = document.getElementById("zuidAmerica");

zuidAmerica.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(BirdLife) + template(TheMountainInstitute) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth);
    $("#verbergen").toggleClass();
});

var europe = document.getElementById("europe");

europe.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(Oxfam) + template(WWF) + template(RedCross) + template(GreenPeace) + template(Gaia) + template(SeaShepherd) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);;
    $("#verbergen").toggleClass();
});

var africa = document.getElementById("africa");

africa.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(GlobalWitness) + template(BirdLife) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalHand) + template(InternationalResuceCommittee);
    $("#verbergen").toggleClass();
});

var azie = document.getElementById("azie");

azie.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(PlanInternational) + template(RedCross) + template(GreenPeace) + template(WomensAidOrganisation) + template(GlobalWitness) + template(BirdLife) + template(TheMountainInstitute) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
    $("#verbergen").toggleClass();
});

var oceanie = document.getElementById("oceanie");

oceanie.addEventListener("click", function() {
    document.querySelector('#users').innerHTML = template(AmnestyInternational) + template(Unicef) + template(DoctorsWithoutBorders) + template(Oxfam) + template(WWF) + template(FoodAid) + template(RedCross) + template(GreenPeace) + template(GlobalWitness) + template(NatureServe) + template(TheWanderingSamaritan) + template(AdvocatsForWorldHealth) + template(InternationalResuceCommittee);
    $("#verbergen").toggleClass();
});

